# Content

This repo contains a collection of R functions for TargetSearch that are either too
specific to include in TargetSearch, or highly experimental functions.