# Script 3.
# Make plots of found metabolites and prepera pre Annotation table

options(stringsAsFactors=FALSE)
library(TargetSearch)
library(RColorBrewer)
library(pbapply)

source("configuration.R")

load("TargetSearch-RImatrix.RData")
load("TargetSearch_First_Run.RData")

# plot peaks on the RT dimension
do_chk_plots <- function(nfo, smpInfo, refLib, pdfname='checkPeakPreAnnotation.pdf')
{
    G <- smpInfo[, sample_group_col]
    G <- if(is.null(exclude_groups)) seq(length(smpInfo)) else ! G %in% exclude_groups

    pdf(pdfname, 10.5, 7)
    pb <- startpb(0, nrow(nfo))
    on.exit({
        dev.off()
        closepb(pb)
    })
    i <- 1
    for(id in nfo$libID) {
        tmp <- tryCatch(plotPeakRI(smpInfo, refLib, id, dev=NA), error=function(e) e)
        if("simpleError" %in% is(tmp)) {
            message("An error occurred processing comp_id = ", id, ". Skipped")
        }
        else if(!is.null(tmp)) {
            nfo$medianRI[i] <- median(tmp, na.rm=TRUE)
            nfo$Count[i] <- sum( is.finite( tmp[G] ) )
        }
        setpb(pb, i)
        i <- i + 1
    }
    nfo
}

do_pdf_plots <- function(nfo, smpInfo, refLib, corRI, path)
{
    pb <- startpb(0, nrow(nfo))
    on.exit(closepb(pb))
    i <- 1
    for(id in nfo$libID) {
        ret <- tryCatch(pltPeakPDF(smpInfo, refLib[id], corRI, nfo=nfo, plotPath=path),
                        error=function(e) e)
        if("simpleError" %in% is(ret)) {
            message("An error ocurred processing comp_id = ", id,
                    ". The corresponding file might be corrupted. Skipped.")
        }
        setpb(pb, i)
        i <- i + 1
    }
}

nfo <- do_chk_plots(nfo, smpInfo, refLib)

# save table for manual annotation
tmp <- nfo[, c('libID', 'Name', 'medianRI', 'Count')]
tmp$RIdev <- RIdev(refLib)[match(tmp$libID, refLib$libID), 1]
tmp$Method <- 'RI'
tmp$Mass <- quantMass(refLib)[ rownames(tmp) ]
tmp$Keep <- TRUE
tmp$Comment <- ""

if(add_intensity)
    tmp <- cbind(tmp, met[ rownames(tmp), ])

write.table(tmp, file="TargetSearch_preAnnotation.txt", sep="\t", quote=FALSE, row.names=FALSE)
# update settings for shiny
source("functions/shiny_helpers.R")
update_settings(tmp)

# plot chromatographic peaks
if(all(peak_plots)) {
    source("functions/plotAllPeaks.R")
    do_pdf_plots(nfo, smpInfo, refLib, corRI, plot_path)
}

# vim: set ts=4 sw=4 et:
