options(stringsAsFactors=FALSE)
source("configuration.R")
source("functions/utils.R")

library(TargetSearch)
library(pbapply)
library(assertthat)

mk_dir(cdf_path)
mk_dir(ri_path)
mk_dir(plot_path)

sampleInfo <- readRDS("sample_info.rds")
smpInfo    <- ImportSamples(sampleInfo, CDFpath=cdf_path, RIpath=ri_path)
sampleNames(smpInfo) <- sampleInfo[, sample_id_col ]

# check that all files exist
stopifnot(file.exists( CDFfiles(smpInfo) ))

# workaround name conflict between `baseline` flag and `baseline` function
if(!exists("baseline")) {
    baseline <- FALSE
} else if(!is.logical(baseline)) {
    baseline <- FALSE
}

message("converting files")
if(baseline) {
    source("functions/baseline_correction.R")
    nil <- pblapply(CDFfiles(smpInfo), cdf_baseline_correct, smoothing=bsl_smoothing,
        quantile=bsl_quant_prob, width=bsl_width, unit=bsl_unit, steps=bsl_steps)
} else {
    smpInfo <- ncdf4Convert(smpInfo)
}

# fame correction
if(exists('fames_file') && is.string(fames_file)) {
    fame <- ImportFameSettings(fames_file)
} else {
    stopifnot(length(fames_rt) == 13)
    fame <- ImportFames(fames_rt, fames_add_left, fames_add_right)
}

# repeat this comand if necessary, by default it
checkRimLim(smpInfo, fame)

RImatrix <- RIcorrect(smpInfo, fame, Window=Window, IntThreshold=100, showProgressBar=TRUE)
outliers <- FAMEoutliers(smpInfo,  RImatrix)

save(fame, RImatrix, outliers, smpInfo, file='TargetSearch-RImatrix.RData')
