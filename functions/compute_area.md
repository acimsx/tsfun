
# Peak Area Computation Tutorial

This simple tutorial shows how to extract peak area from GC peaks using `TargetSearch`

## Requirements

You need:

* This source repository, in particular the file `compute_area.R`.
* The file `TargetSearch-RImatrix.RData` which is created with this workflow.
* The **targets** file. See below.


## Targets File

The **targets** file is a tab-delimited file that contains a list of metabolites
to extract the peak area for. This file needs the columns below. Note that
the case is sensitive!

* `Peak_ID` the peak identifier. The only requirement is that it is unique.
* `Compound_Name` the name of the metabolite or analyte.
* `Quant_Mass` the *m/z* to search for (the quantification mass).
* `RI` the retention time index (RI) or retention time (RT). Whether it is one
   or the other, it is defined in the script below.
* `RI_window` the time window to search for the apex of the peak. The units must match
  the units of the `RI` column.
* `Peak_width` is the maximum allowed peak width. Again, the time units must match.
* `Int_threshold` A percentage of the peak height used to estimate the peak boundaries.


An example target file is shown below.

| Peak_ID | Compound_Name | Quant_Mass | RI  | RI_window | Peak_width | Int_threshold |
| ------- | ------------- | ---------- | --- | --------- | ---------- | ------------- |
| GC_01 | lactic acid | 117 | 192000 | 2000 | 10000 | 2 |
| GC_02 | alanine | 116 | 206000 | 2000 | 10000 | 2 |


Note that the *peak width* is defined as the whole peak, while the *search window*
is plus or minus from the apex.

The `Peak_ID` and `Compound_Name` can be anything. The former is used for `PDF` file
names, while the latter is used only plotting.


## Algorithm

For each metabolite and each file, the apex is searched within the search window
(in the case of *alanine* above, this window is from 204000 to 208000). Then,
the function looks left and right from the apex for the peak border until the
intensity drops below the *intensity threshold*, which will be considered as the
peak border. However, if half the *peak width* is reached before such drop is
found, then the minimum intensity is assigned as the border.


## Step by step script

Load the functions, the `RData` file and `TargetSearch`. This assumes that the files
are in the current directory, otherwise change directories with `setwd`.

```R
require(TargetSearch)
source('functions/compute_area.R')
load('TargetSearch-RImatrix.RData')
```

Load the target file and define whether the time units are *retention time index*
or just *retention time*

```R
# import the targets file
def <- read.delim("targets.txt")

# set to TRUE if using RT. If FALSE, it means RI.
useRT <- FALSE
```

The following computes the peak area and makes a PDF file for each metabolite in
the working directory. The PDF files are named after the peak IDs, so they must
be unique to avoid name collisions.

```R
# get a list of CDF files (nc4) from the sample info object.
cdf <- CDFfiles(smpInfo)
names(cdf) <- sampleNames(smpInfo)

# compute area for each CDF file
ret <- sapply(cdf, compute_area, def, useRT=useRT, simplify=FALSE)

# make plots
make_area_plots(ret, def, useRT=useRT)

# extract the computed area into a matrix
result <- sapply(ret, getElement, 'Area')

# this makes sure a matrix is created (it happens when there's only 1 target)
if(!is.matrix(result))
    result <- matrix(result, nrow=1, dimnames=list(def$Peak_ID, names(result)))
```

Finally, save the targets into a text file.

```R
write.table(result, file='peak_area.txt', sep="\t", quote=FALSE)
```

## Adjustments

You need to verify that the peak area was computed correctly by checking
the generated PDF files. Try increasing or decreasing the *peak_width* and
*intensity threshold* parameters until the area is picked correctly.
Unfortunately, this is not possible to do this automatically.
