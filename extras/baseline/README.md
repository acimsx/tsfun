## Baseline Correction

This configuration example contains an optional baseline
correction method (normally not needed).

Warning: requires developmental version of TargetSearch (>= 1.41.2)
